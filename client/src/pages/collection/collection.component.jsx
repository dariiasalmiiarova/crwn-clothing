import React, { useEffect } from 'react';
import { connect } from 'react-redux';

// import {firestore} from '../../firebase/firebase.utils';
import { selectCollection } from '../../redux/shop/shop.selectors';
import CollectionsItem from '../../components/collection-item/collection-item.component';

import './collection.styles.scss';

//we have access to match props bc we use this component in Route
const CollectionPage = ({ collection }) => {
    const { title, items } = collection;
    // useEffect(() => {
    //     console.log('I am subscribing!');
    //     const unsubscribeFromCollection = firestore.collection('collections').onSnapshot(snapshot => console.log(snapshot));
    //     return() => {
    //         console.log('I am unsubscribing!');
    //         unsubscribeFromCollection();
    //     }
    // }, []);
    return(
        <div className="collection-page">
            <h2 className="title"> { title } </h2>
            <div className="items">
                {
                    items.map(item => (<CollectionsItem key={item.id} item={item} />))
                }
            </div>
        </div>
    ); 
};

const mapStateToProps = (state, ownProps) => ({
    //unlike other selectors, this selector needs a part of the state depending on the URL parametr
    collection: selectCollection(ownProps.match.params.collectionId)(state)
});

export default connect(mapStateToProps)(CollectionPage);