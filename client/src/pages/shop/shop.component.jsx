import React, { useEffect, lazy, Suspense } from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';

import { fetchCollectionsStart } from '../../redux/shop/shop.actions';

import Spinner from '../../components/spinner/spinner.component';

// const CollectionPageWithSpinner = WithSpinner(CollectionPage);
const CollectionContainer = lazy(() => import('../collection/collection.container'));
const CollectionOverviewContainer = lazy(() => import('../../components/collections-overview/collection-overview.container'))

const ShopPage = ({ fetchCollectionsStart, match }) => {
    useEffect(() => { 
        fetchCollectionsStart();
    }, [fetchCollectionsStart])

    // const { updateCollections } = this.props;
        // const collectionRef = firestore.collection('collections');

        // //Using firebase coludstore as rest api
        // // fetch( 'https://firestore.googleapis.com/v1/projects/crwn-db-4c9f2/databases/(default)/documents/collections')
        // // .then(response => response.json())
        // // .then(collections => console.log(collections));

        // //Api call to fetch the data that we needed
        // collectionRef.get().then(snapshot => {
        //     const collectionMap = convertCollectionSnapshotToMap(snapshot)
        //     updateCollections(collectionMap);
        //     this.setState({ loading: false });
        // })

        // // this.unsubscribeFromSnapshot = collectionRef.onSnapshot( async snapshot => {
        // //     const collectionMap = convertCollectionSnapshotToMap(snapshot)
        // //     updateCollections(collectionMap);
        // //     this.setState({ loading: false });
        // // });
    return(
        <div className="shop-page">
            <Suspense fallback={ <Spinner /> }>
                <Route exact path={`${match.path}`} component={CollectionOverviewContainer} />
                {/* here we passing ownProps to CollectionPage */}
                <Route path={`${match.path}/:collectionId`} component={CollectionContainer} />
            </Suspense>
        </div>
    )
}


const mapDispatchToProps = dispatch => ({
    fetchCollectionsStart: () => dispatch(fetchCollectionsStart())
})

export default connect(null, mapDispatchToProps)(ShopPage);