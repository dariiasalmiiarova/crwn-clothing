import React, { Profiler } from 'react'

import Directory from '../../components/directory/directory.component';
// import './homepage.style.scss';

import { HomePageContainer } from './homepage.styles';

const HomePage = () =>(
    <HomePageContainer >
        <Profiler id="Directory" onRenser={(id, phase, actualDuration) => {
            console.log({ id, phase, actualDuration });
        }}>
            <Directory />
        </Profiler>
    </HomePageContainer>
)
export default HomePage;