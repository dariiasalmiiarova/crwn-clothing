import styled from 'styled-components';

export const HomePageContainer = styled.div`
    width: 90%;
    margin: auto;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 20px 80px;
`;