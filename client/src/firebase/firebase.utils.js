import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyBkmznZMHk3nA3EsZ9gsYfgxhB5rl2yaQU",
    authDomain: "crwn-db-4c9f2.firebaseapp.com",
    databaseURL: "https://crwn-db-4c9f2.firebaseio.com",
    projectId: "crwn-db-4c9f2",
    storageBucket: "crwn-db-4c9f2.appspot.com",
    messagingSenderId: "529180304402",
    appId: "1:529180304402:web:660cc3d0dfc51e56f264cb",
    measurementId: "G-NVZFX7CYPT"
};

//updating the firestore database
export const createUserProfileDocument = async(userAuth, additionalData) =>{
    //if user object returns null, doesn't exist
    if(!userAuth) return;
    const userRef = firestore.doc(`users/${userAuth.uid}`);

    //getting an array of users
    // const collectionRef = firestore.collection('users');
    // const collectionSnapshot = await collectionRef.get();
    // console.log({ collections: collectionSnapshot.docs.map(doc => doc.data()) }); //using .data to get json representation of the values

    const snapShot = await userRef.get();
    //checking if snapShot exists or not which insure that we don't multiply the same user

    //-----------------------------------------------------
    // creating new user in database
    if(!snapShot.exists){
        const { displayName, email } = userAuth;
        const createdAt = new Date();
        try{
            await userRef.set({
                displayName,
                email,
                createdAt,
                ...additionalData
            })
        }catch(error){
            console.log('error creating user', error.message);
        }
    }

    return userRef;
};

export const addCollectionAndDocuments = async (collectionKey, objectsToAdd) => {
    const collectionRef = firestore.collection(collectionKey);

    //Batch - is a way to group all of our calls together(bc we can't have 2 calls in the same time without it)
    const batch = firestore.batch();
    objectsToAdd.forEach(obj => {
        const newRef = collectionRef.doc();
        batch.set(newRef, obj);
        // newRef.set(newRef, obj);
    });

    return await batch.commit();
};

export const convertCollectionSnapshotToMap = (collections) => {
    const transformedCollection = collections.docs.map(doc => {
        const { title, items } = doc.data(); //got it from doc.data

        return{
            routeName: encodeURI(title.toLowerCase()),
            id: doc.id,
            title,
            items
        };
    });

    //меняем буквы в названии на маленькие и приписываем их в collection as a key,
    //because title we got from doc and we need to change it on the other method
    //как и for, reduce позволяет изменить каждый обьект масива
    return transformedCollection.reduce((accumulator, collection) => {
        accumulator[collection.title.toLowerCase()] = collection;
        return accumulator;
    }, {});
    // console.log(transformedCollection);
};

export const getCurrentUser = () => {
    return new Promise((resolve, reject) => {
        const unsubscribe = auth.onAuthStateChanged = (userAuth => {
            unsubscribe();
            resolve(userAuth);
        }, reject)  
    })
}



firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

//gives access to Google Auth Provider class in auth library
export const googleProvider = new firebase.auth.GoogleAuthProvider();
googleProvider.setCustomParameters({ prompt: 'select_account' });
//sign in popup for google
export const signInWithGoogle = () => auth.signInWithPopup(googleProvider);

export default firebase;