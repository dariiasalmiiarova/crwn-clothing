import React, { useEffect, lazy, Suspense } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
// import { setCurrentUser } from './redux/user/user.action'; //our Action to manage state
import { createStructuredSelector } from 'reselect';
import { selectCurrentUser } from './redux/user/user.selector';
import { checkUserSession } from './redux/user/user.action';

import { GlobalStyle } from './global.styles';

import Header from './components/header/header.component';
import Spinner from './components/spinner/spinner.component';
import ErrorBoundary from './components/error-boundary/error-boundary.component';

const HomePage = lazy(() => import('./pages/homepage/homepage.component'));
const ShopPage = lazy(() => import('./pages/shop/shop.component'));
const CheckoutPage = lazy(() => import('./pages/checkout/checkout.component'));
const SignInandSignUp = lazy(() => import('./pages/login-and-registration/sign-in-and-up.component'));

const App = ({ checkUserSession, currentUser }) => {

  //listener to authentication state changing
  // unsubscribeFromAuth = null;

  useEffect(() => { 
    checkUserSession();
  }, [checkUserSession])

  // componentWillUnmount(){ 
  //   this.unsubscribeFromAuth();
  // }

  return (
    <div>
      <GlobalStyle />
      {/* Placing Header outside switch and routes gives the option that header is always present no matter what's rendering */}
      <Header />
        <Switch>
          <ErrorBoundary>
            <Suspense fallback={<Spinner />}>
              <Route exact path='/' component={HomePage}/>
              <Route path='/shop' component={ShopPage}/>
              <Route path='/checkout' component={CheckoutPage}/>
              {/* If user signed in he hasn't access to login page*/}
              <Route exact path='/signinandsignup' render={() => currentUser ? (<Redirect to='/'/>) : (<SignInandSignUp />)} />
            </Suspense>
          </ErrorBoundary>
        </Switch>
    </div>
  );
}

//декларируем нужное состояние - текущее состояніє
//user - is from root-reducer

// const mapStateToProps = state => ({
//   currentUser: state.user.currentUser
// })
const mapStateToProps = createStructuredSelector({
  currentUser: selectCurrentUser
});

const mapDispatchToProps = dispatch => ({
  checkUserSession: () => dispatch(checkUserSession())
})
//передаем текущее состояние в действие
// const mapDispatchToProps = dispatch => ({
//   //мы в действие передаем состояние user
//   //setCurrentUser is a function that gets user, which calls dispatch, in dispatch we're calling our action and passed in user(changed)
//   setCurrentUser: user => dispatch(setCurrentUser(user))
// })
//mapStateToProps переданый в connect - позволяет подписываться на событие
export default connect(mapStateToProps, mapDispatchToProps)(App);
 