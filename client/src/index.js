import React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import * as serviceWorker from './serviceWorker';

import './index.css';

import App from './App';
import {store, persistor } from './redux/store';

ReactDOM.render(
    // Provider - делает состояние Redux доступным для всех компонентов
    <Provider store={store}>
        <BrowserRouter >
        {/* persistor - persisted version of our store */}
            <PersistGate persistor = {persistor}>
                <App />
            </PersistGate>
        </BrowserRouter>
    </Provider>, document.getElementById('root')
);

serviceWorker.register();