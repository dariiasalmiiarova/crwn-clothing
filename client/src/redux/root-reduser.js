//This is the main Reduser what combines our all states together
import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
//telling that we want to use local storage as a default
import storage from 'redux-persist/lib/storage';

import userReduser from './user/user.reducer';
import cartReducer from './cart/cart.reducer';
import directoryReducer from './directory/directory.reducer';
import shopReducer from './shop/shop.reducer';

const persistConfig = {
    key: 'root',
    storage,
    //whitelist - we giving names of reducers that we whant to store, 
    //userReducer is already heald by firebase
    whitelist: ['cart']
}

const rootReducer = combineReducers({
    user: userReduser,
    cart: cartReducer,
    directory: directoryReducer,
    shop: shopReducer
});

export default persistReducer(persistConfig, rootReducer);