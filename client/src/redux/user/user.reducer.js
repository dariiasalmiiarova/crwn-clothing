import UserActionTypes from './user.types';

const INITIAL_STATE = {
    currentUser: null
}
//if the state is never changed or just rendered, we need default value, which our INITIAL_STATE is
const userReduser = (state = INITIAL_STATE, action) => {
    switch(action.type){
        case UserActionTypes.SIGN_IN_SUCCESS:
            return {
//means we return(copy) current state(everything else on the state), cause we want to modify only a state of current_user
                ...state,
                currentUser: action.payload,
                error: null
            }
        case UserActionTypes.SIGN_OUT_SUCCESS:
            return {
                ...state,
                currentUser: null,
                error: null
            }
        case UserActionTypes.SIGN_IN_FAILURE:
        case UserActionTypes.SIGN_OUT_FAILURE:
        case UserActionTypes.SIGN_UP_FAILURE:
            return{
                ...state,
                error: action.payload
            }
        default:
            return state;
    }
}

export default userReduser;