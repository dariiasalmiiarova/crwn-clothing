import { createStore, applyMiddleware } from 'redux';
import { persistStore } from 'redux-persist';
//Middleware - smth that resives actions and desides what to do with that
import logger from 'redux-logger';
// import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';

import rootReduser from './root-reduser';
import rootSaga from './root-saga';

const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware];

if(process.env.NODE_ENV === 'development'){
    middlewares.push(logger);
}

//in store you pass the reducer, that's why we have the root-reducer which combines all our reducers together
export const store = createStore(rootReduser, applyMiddleware(...middlewares));

sagaMiddleware.run(rootSaga);

export const persistor = persistStore(store);

export default {store, persistor};