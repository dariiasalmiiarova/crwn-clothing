import React from 'react';

import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import CartIcon from '../cart-icon/cart-icon.component';
import CartDropdown from '../cart/cart-dropdown.component';
import { selectCurrentUser } from '../../redux/user/user.selector';
import { selectCartHidden } from '../../redux/cart/cart.selectors';
import { signOutStart } from '../../redux/user/user.action';

import { ReactComponent as Logo } from '../../assets/original.svg';
// import './header.styles.scss';
import { HeaderContainer, LogoContainer, OptionsContainer, OptionLink } from './header.styles';
// ({currentUser}) -  destructuring props
const Header = ({currentUser, hidden, signOutStart}) =>(
    <HeaderContainer >
        <LogoContainer  to ="/">
            <Logo className='logo'/>
        </LogoContainer>
        <OptionsContainer >
            <OptionLink  to='/shop'>SHOP</OptionLink>
            <OptionLink  to='/shop'>CONTACT</OptionLink> 
            {
                currentUser ? 
                <OptionLink as='div'  onClick={ signOutStart }>SIGN OUT</OptionLink>
                :
                <OptionLink to='/signinandsignup'>SIGN IN</OptionLink>
            }
            <CartIcon />
        </OptionsContainer>
        {
            hidden ? null : <CartDropdown />
        }
    </HeaderContainer>
);

//here we getting a state of current user and passing it in Header commponent as a props
//of of our state we want the user, and from the user we want the currentUser 
const mapStateToProps = createStructuredSelector({
    currentUser: selectCurrentUser,
    hidden: selectCartHidden
})

const mapDispatchToProps = dispatch => ({
    signOutStart: () => dispatch(signOutStart()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Header);
//first argument that we passed to connect() is the function that allows us to access the state(root-reduser)