import React from 'react'

import Spinner from '../spinner/spinner.component';

//WithSpinner component is a function takes some component(WrappedComponent), that we want to wrap with this funtionality
const WithSpinner = WrappedComponent => ({ isLoading, ...otherProps }) => {
    //If it's loading make css animation, if not just pass the props
    return isLoading ? (
        <Spinner />
    ) : (
            //Just passing through this component other props
        <WrappedComponent {...otherProps} />
    );
};

export default WithSpinner;