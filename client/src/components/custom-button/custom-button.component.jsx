import React from 'react';


import { CustomButtonContainer } from './custom-button.styles';

const CustomButton = ({children, ...props}) =>(
    //adding class depending on the given props
    <CustomButtonContainer {...props} >
        {children}
    </CustomButtonContainer>
)

export default CustomButton;