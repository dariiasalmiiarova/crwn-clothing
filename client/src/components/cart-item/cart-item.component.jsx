import React from 'react';

import './cart-item.styles.scss';
//деструктурирукм данные, которые нам нужны в корзине
//item передается из cart dropdown, который достает это из state
const CartItem = ({ item: { imageUrl, price, name, quantity } }) => (
    <div className="cart-item">
        <img src={imageUrl} alt="item"/>
        <div className="item-details">
            <span className="name">{name}</span>
            <span className="price">{quantity} x ${price}</span>
        </div>
    </div>
)

export default React.memo(CartItem);